package com.indrek.testing.weektwo.tests;

import com.indrek.testing.Utils.DriverManager;
import com.indrek.testing.weektwo.pageobjects.WeekTwoPageObjects;
import org.testng.Assert;
import org.testng.annotations.Test;

public class WeekTwo extends DriverManager {

    @Test
    public void testFobLogoIsDisplayed() {
        Assert.assertTrue(new WeekTwoPageObjects(DriverManager.getWd()).getFobLogo().isDisplayed(), "Fob Solutions logo is displayed");
    }
}
