package com.indrek.testing.weektwo.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WeekTwoPageObjects{
    WebDriver wd;
    public WeekTwoPageObjects(WebDriver driver){
        wd = driver;
    }
    private By fobLogo = By.className("company_logo_wrapper");

    public WebElement getFobLogo(){
        return wd.findElement(fobLogo);
    }
}
