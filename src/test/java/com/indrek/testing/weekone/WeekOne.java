package com.indrek.testing.weekone;

import com.indrek.app.RandomGenerator;
import org.testng.annotations.Test;

import static org.junit.Assert.assertTrue;

public class WeekOne {

    @Test
    public void correctResultForInput() {
        int minValue = 40;
        int maxValue = 50;
        int random = RandomGenerator.getRandomNumber(minValue, maxValue);
        assertTrue("The output should be in the range of " + minValue + " and max value " + maxValue,
                random >=40 && random<=50 );
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void maxIsLargerThanMin() {
        int invalidMin = 12000;
        int max = 4000;
        RandomGenerator.getRandomNumber(invalidMin, max);
    }

    @Test
    public void maxCanEqualMin() {
        int min = 4000;
        int max = 4000;
        assertTrue("Max can equal min", RandomGenerator.getRandomNumber(min, max) == min);
    }
}
