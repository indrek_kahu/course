package com.indrek.testing.Utils;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class DriverManager {

    public static WebDriver wd;

    @BeforeTest
    public void executeBeforeTest() {
        ChromeDriverManager.getInstance().setup();
        wd = new ChromeDriver();
        wd.get(Constants.BASE_URL);
    }

    public static WebDriver getWd() {
        return wd;
    }

    @AfterTest
    public void executeAfterTest() {
        wd.quit();
    }
}
