package com.indrek.app;

import java.util.Random;

public class RandomGenerator {

    public static void main(String[] args) {
        int max = 9999;
        int min = 0;
        System.out.print(getRandomNumber(min, max));
    }

    // Return random int between given boundaries. Throw exception if max is not larger than min
    public static int getRandomNumber(int min, int max) {
            Random random = new Random();
            return random.nextInt((max - min) + 1) + min;
    }
}
