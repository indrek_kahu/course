To run tests for this application
1. Clone this repository
2. Open terminal
3. Navigate to project folder
4. Running unit tests
    1) For windows run command 'gradlew test' to run the tests
    2) For OSX run command 'gradle test' to run the tests
5. For running single test
    1) For windows run command 'gradlew test --tests *TestName' to run the tests
    2) For OSX run command 'gradle test --tests *TestName' to run the tests

Test results are generated to build folder in the project source folder